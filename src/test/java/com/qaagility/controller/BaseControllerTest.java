package com.qaagility.controller;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.ui.ModelMap;

public class BaseControllerTest {

	@Test
	public void testWelcome() {
		BaseController baseController= new BaseController();
		ModelMap model = new ModelMap();
		String welcome = baseController.welcome(model);
		assertEquals(welcome, "index");
	}

	@Test
	public void testWelcomeName() {
		BaseController baseController= new BaseController();
		ModelMap model = new ModelMap();
		String welcome = baseController.welcomeName("Prajot", model);
		assertEquals(welcome, "index");
	}

}
